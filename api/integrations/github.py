import os
import requests


class GithubApi:
    API_URL = os.environ.get("GITHUB_API_URL", "https://api.github.com")
    GITHUB_TOKEN = os.environ.get("GITHUB_TOKEN")

    def get_organization(self, login: str):
        """Busca uma organização no Github

        :login: login da organização no Github
        """
        headers = {'Authorization': f'Basic {self.GITHUB_TOKEN}'}
        repo = requests.get(f'{self.API_URL}/orgs/{login}', headers=headers)
        if repo.status_code == 200:
            return {
                'login': repo.json()['login'],
                'name': repo.json()['name'],
                'public_repos': repo.json()['public_repos'],
            }
        return None

    def get_organization_public_members(self, login: str) -> int:
        """Retorna todos os membros públicos de uma organização

        :login: login da organização no Github
        """
        headers = {'Authorization': f'Basic {self.GITHUB_TOKEN}'}
        repo = requests.get(f'{self.API_URL}/orgs/{login}/public_members', headers=headers)
        if repo.status_code == 200:
            return len(repo.json())

        return 0
