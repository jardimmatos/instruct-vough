import os

import requests
from django.test import TestCase


class ConsultaOrganizacaoTeste(TestCase):
    API_BASE = os.environ.get('API_BASE','http://localhost:8000')
    GITHUB_TOKEN = os.environ.get('GITHUB_TOKEN')

    def setUp(self):
        # Setup run before every test method.
        pass

    def nao_existe_org(self):
        get_org = requests.get(f'{self.API_BASE}/api/orgs/abcdefgh1234567x')
        self.assertTrue(get_org.status_code == 404)

    def existe_org(self):
        get_org = requests.get(f'{self.API_BASE}/api/orgs/github')
        self.assertTrue(get_org.status_code == 200)

    def deletar_existente(self):
        del_org = requests.delete(f'{self.API_BASE}/api/orgs/github')
        self.assertTrue(del_org.status_code == 204)

    def listar_orgs(self):
        get_orgs = requests.get(f'{self.API_BASE}/api/orgs')
        self.assertTrue(get_orgs.status_code == 200)



