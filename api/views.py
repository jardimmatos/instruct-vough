import requests
from rest_framework import viewsets, status
from rest_framework.authentication import BasicAuthentication
from rest_framework.views import Response

from api import models, serializers
from api.integrations.github import GithubApi

# TODOS:
# 1 - Buscar organização pelo login através da API do Github
# 2 - Armazenar os dados atualizados da organização no banco
# 3 - Retornar corretamente os dados da organização
# 4 - Retornar os dados de organizações ordenados pelo score na listagem da API


class OrganizationViewSet(viewsets.ModelViewSet):

    queryset = models.Organization.objects.all()
    serializer_class = serializers.OrganizationSerializer
    lookup_field = "login"
    authentication_classes = (BasicAuthentication,)

    def destroy(self, request, *args, **kwargs):
        # aceitar case insensitive opcional não definido como requisito para o teste
        instance = self.queryset.filter(login__iexact=kwargs.get('login')).first()
        if instance:
            self.perform_destroy(instance)
            return Response(status=status.HTTP_204_NO_CONTENT)

        return Response(status=status.HTTP_404_NOT_FOUND)


    def perform_destroy(self, instance):
        instance.delete()

    def list(self, request):
        lista_orgs = self.queryset.order_by('-score')
        serializer = self.get_serializer(lista_orgs, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def retrieve(self, request, login=None):
        git_hub_api = GithubApi()
        org = git_hub_api.get_organization(login)
        if org:
            # Qtde de Membros Públicos
            count_public_members = git_hub_api.get_organization_public_members(login)

            # Qtde de repositórios públicos
            count_public_repos = org['public_repos']

            # Cálculo de Prioridade
            prioridade = count_public_members + count_public_repos

            id_login = org['login']
            name = org['name']
            # atualizar ou criar nova organização
            organization, created = self.queryset.update_or_create(
                login__iexact=id_login,
                defaults={'login': id_login, 'name': name, 'score': prioridade},
            )
            # retornar os dados serializados
            serializer = self.get_serializer(organization)

            return Response(serializer.data, status=status.HTTP_200_OK)

        # retorno padrão como Não encontrado
        return Response({}, status=status.HTTP_404_NOT_FOUND)

