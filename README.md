## Objetivo da API
API que calcula o valor de prioridade de um cliente e retorna uma lista de clientes ordenandos por prioridade.
O valor de prioridade é calculado com base em dados encontrados no Github, através da fórmula:

`prioridade = <quantidade de membros públicos da organização no Github> + <quantidade de repositórios públicos da organização no Github>`


## Configuração do ambiente Virtual

1. #### criação da venv
    ```python -m venv venv```

2. #### ativando a venv
    - Windows
    ```source venv\Script\activate```
    - Linux ou Mac
    ```source venv\bin\activate```

3. #### instalação dos pacotes necessários
    - pacotes no arquivo ```requirements.txt```

        ```pip install -r requirements.txt```
    
4. #### Servidor
     ```python manage.py runserver```

## Requisições API
1. #### Por Lista
    - Retorno endpoint  ```GET /api/orgs```
        ```
        [
          {
            "login": "github",
            "name": "GitHub",
            "score": 402
          },
          ...
        ]
        ```
2. #### Por Login
    - Retorno endpoint  ```GET /api/orgs/<login>/```
        ```
        {
            "login": "[login]",
            "name": "[login name]",
            "score": [score login]
        }
        ```

3. #### Exclusão
    - Endpoint ```DELETE /api/orgs/<login>/``` 
      
      - status ```204``` para organização existente
      - status ```404``` para organização inexistente
    

## Testes
   - Testar Organização inexistente retornando status ```404```.

        ```python manage.py test api.tests.ConsultaOrganizacaoTeste.nao_existe_org```

   - Testar Organização existente retornando status ```200```

        ```python manage.py test api.tests.ConsultaOrganizacaoTeste.existe_org```

   - Testar Organização existente retornando status ```204```

        ```python manage.py test api.tests.ConsultaOrganizacaoTeste.deletar_existente```

- Testar listagem de Organizações retonando status ```200```

    ```python manage.py test api.tests.ConsultaOrganizacaoTeste.listar_orgs```

## Variáveis de ambiente
```API_BASE = 'http://localhost:8000' (exemplo)```

```GITHUB_TOKEN = 'xxxxxxxxxxxxxx'```


#
##### Documentação API completa em
https://app.swaggerhub.com/apis/jardimmatos/Instruct-vough2/1.0.0
